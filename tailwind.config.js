// breakpoints: 768, 950, 1200, 1500
// md, half-fd (lg), xl, 2xl

// tailwind.config.js
module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: {
    // enable manual purge of extra css styles
    enabled: true,
    content: ['./src/**/*.php'],
  },
  theme: {
    fontFamily: {
      display: ['Roboto Condensed', 'Roboto', 'sans-serif'],
      'ma-icons': ['Material Icons'],
      body: ['Roboto', 'sans-serif'],
    },
    extend: {
      colors: {
        'graa': 'rgb(242, 242, 242)',
        //'primary': 'rgb(17, 78, 125)',
        'primary': 'rgb(2, 123, 178)',
        'secondary': '#2067a9',
        'accent': '#117d40',
        'cta': '#f07d00',
        'kampanjat': '#0b977b',
      },
      zIndex: {
        'bottom': '-999',
        'top': '999',
        'nav': '99',
        'burger-bg': '105',
        'burger': '110',
        '100': '100',
      },
      minHeight: {
        '20': '5rem',
        '60': '15rem',
        '80': '20rem',
        '100': '25rem',
        '120': '30rem',
        '220': '55rem',
      },
      maxHeight: {
        '80': '20rem',
        '100': '25rem',
        '128': '32rem',
      },
      width: {
        '40': '10rem',
        '44': '11rem',
        '52': '13rem',
        '60': '15rem',
        '80': '20rem',
        '160': '40rem',
        '180': '45rem',
        '200': '50rem',
        '15p': '15%',
        '80p': '80%',
        '85p': '85%',
        '90p': '90%',
        'mobile-logo': '160px',
        'bookers': '38.3rem',
      },
      minWidth: {
        '120': '30rem',
      },
      maxWidth: {
        '120': '30rem',
        '140': '38rem',
        'card': '34.5rem',
      },
      inset: {
        '2': '0.5rem',
        '4': '1rem',
        '8': '2rem',
        '16': '4rem',
        '20': '5rem',
        '24': '6rem',
      },
      margin: {
        '-1.5': '-0.375rem',
      },
      padding: {
        '1.5': '0.375rem',
      },
      lineHeight: {
        'extra-loose': 2.5,
      },
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '950px',
      'xl': '1200px',
      '2xl': '1500px',
    },
    fontSize: {
      'sm': '1.3rem',
      'para': '1.5rem',
      'md': '1.6rem',
      'base': '1.6rem',
      'lg': '1.9rem',
      'xl': '2.2rem',
      '2xl': '2.8rem',
      '3xl': '3.4rem',
      '4xl': '4rem',
    },
    boxShadow: {
      default: '0 1px 3px 0 rgba(0, 0, 0, .1), 0 1px 2px 0 rgba(0, 0, 0, .06)',
      md: '0 4px 6px -1px rgba(0, 0, 0, .1), 0 2px 4px -1px rgba(0, 0, 0, .06)',
      lg: '0 10px 15px -3px rgba(0, 0, 0, .1), 0 4px 6px -2px rgba(0, 0, 0, .05)',
      xl: '0 20px 25px -5px rgba(0, 0, 0, .1), 0 10px 10px -5px rgba(0, 0, 0, .04)',
      '2xl': '0 25px 50px -12px rgba(0, 0, 0, .25)',
      '3xl': '0 35px 60px -15px rgba(0, 0, 0, .3)',
      inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.06)',
      outline: '0 0 0 5px #4e008e',
      focus: '0 0 0 5px rgba(0, 0, 225, 0.5)',
      'none': 'none',
    }
  },
  variants: {
    // extend doesn't work for some reason
    backgroundColor: ['responsive', 'group-hover', 'focus-within', 'hover', 'focus', 'odd'],
    margin: ['responsive', 'last'],
    filter: ['responsive', 'hover', 'focus'],
    brightness: ['responsive', 'hover', 'focus'],
  },
}
