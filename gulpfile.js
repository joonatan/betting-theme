const postcss = require('gulp-postcss');
const gulp = require('gulp');
const { series } = require('gulp');
const copy = require('gulp-copy');

const concat = require('gulp-concat');
const tar = require('gulp-tar');
const gzip = require('gulp-gzip');

const uglifycss = require('gulp-uglifycss');

const run = require('gulp-run');
const rename = require('gulp-rename');

const OUTPUT_DIR = 'build'        // output directory for transpiled code
const SRC_DIR = 'src'             // source code directory
const PKG_NAME = 'betting-theme'  // tar package name without extension

const package = () => (
  gulp.src(`${OUTPUT_DIR}/**/*`)
    .pipe(tar(`${PKG_NAME}.tar`))
    .pipe(gzip())
    .pipe(gulp.dest('dist'))
)

const css = () => (
  gulp.src(`${SRC_DIR}/*.css`)
    .pipe(postcss([
      require('postcss-nesting'),
      require('tailwindcss'),
      require('autoprefixer'),
    ]))
    .pipe(concat({ path: 'style.css', stat: { mode: 0666 }}))
    .pipe(gulp.dest(OUTPUT_DIR))
)

const copyFiles = () => (
  gulp
    .src([`${SRC_DIR}/**/*.php`])
    .pipe(copy(OUTPUT_DIR, { prefix: 1 }))
)

const copyAssets = () => (
  gulp
    .src([`${SRC_DIR}/assets/**/*.*`])
    .pipe(copy(OUTPUT_DIR, { prefix: 1 }))
)

const optimizeCss = () => {
  return gulp
    .src(`${SRC_DIR}/*.css`)
    .pipe(postcss([
      require('postcss-nesting'),
      require('tailwindcss'),
      require('autoprefixer'),
    ]))
    .pipe(concat({ path: 'style.css', stat: { mode: 0666 }}))
    .pipe(uglifycss({ "uglyComments": true }))
    .pipe(gulp.dest(`${OUTPUT_DIR}`))
}

const copyDocker = () => {
  return run('./cp_docker.sh').exec();
}

// TODO optimizeCss hides the Theme details from Wordpress since it removes comments
//exports.package = series(optimizeCss, copyFiles, copyAssets, package)
exports.package = series(css, copyFiles, copyAssets, package)
exports.css = css
exports.default = series(css, copyFiles, copyAssets, copyDocker)
