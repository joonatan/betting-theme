<div id="definitely-not-a-sidebar" class="w-180 font-display">
  <ul>
    <?php
    if (is_active_sidebar('sidebar')):
      dynamic_sidebar('sidebar');
    endif;
    ?>
  </ul>
</div>
