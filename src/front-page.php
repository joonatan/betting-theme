<?php
/**
 * Homepage for Affi theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<?php
$news_count = wp_is_mobile() ? 5 : 7;
$news = get_posts( array(
  'numberposts' => $news_count,
));
?>

<body>
  <div class="mx-0 flex justify-center">

    <main id="main" class="front-page page-2xl flex flex-wrap bg-graa py-2 lg:py-6 lg:px-4" tabindex="-1">
      <!-- show 6 latests news -->
      <?php if (count($news) > 0): ?>
        <div class="w-full md:w-1/2 mb-4 md:m-0">
          <?php
          $post = $news[0];
          setup_postdata( $post );
          get_template_part( 'template-parts/card-element' );
          wp_reset_postdata();
          ?>
        </div>
      <?php endif; ?>

      <?php if (count($news) > 1): ?>
        <div class="w-full md:w-1/2 flex flex-col justify-between">
          <?php
          $news_tail = array_slice($news, 1);
          if ( $news_tail ) {
            foreach ( $news_tail as $post ) {
              setup_postdata( $post );
              get_template_part( 'template-parts/list-element' );
            }
            wp_reset_postdata();
          }
          ?>
          <div class="flex justify-around">
            <a class="btn-sec-base text-sm lg:text-xl" href="/uutiset">
              Lisää Uutisia
            </a>
          </div>
        </div>
      <?php endif; ?>

      <div class="flex flex-col lg:flex-row">
      <div class="w-full font-display flex-shrink-0 lg:w-bookers">
        <div class="mx-2 xl:ml-0 xl:mr-6">
          <h2 class="w-full py-4 text-xl lg:text-2xl text-primary font-bold text-center font-display uppercase"><?php _e('Kuukauden parhaat tarjoukset', 'affi'); ?></h2>
          <?php
          $booker_count = wp_is_mobile() ? 5 : 20;
          echo do_shortcode( "[booker_list small=true count=$booker_count]" );
          ?>
        </div>
      </div>

      <!-- 9 x vihjeet here in a grid pattern next to affi sites -->
      <div class="w-full lg:w-auto flex flex-wrap justify-between">
        <h2 class="w-full py-4 text-xl lg:text-2xl text-primary font-bold text-center font-display uppercase"><?php _e('Uusimmat vihjeet', 'affi'); ?></h2>

        <?php
          $tz = new DateTimeZone('Europe/Helsinki');
          $vihje_count = wp_is_mobile() ? 4 : 15;
          $vihjeet = get_posts( array(
            'post_type' => 'vihje',
            'numberposts' => 20,
            'meta_key'    => 'vihje_expires',
            'meta_type'   => 'DATETIME',
            'orderby'     => 'meta_value',
            'order'       => 'ASC',
            'meta_query' => array(
              array(
                'key'     => 'vihje_expires',
                'value'   => date_format(new DateTime('NOW', $tz), "Y-m-d H:i:s"),
                'compare' => '>',
              ),
            )
          ));
          $vcount = count($vihjeet);

          $vihjeet_old = get_posts( array(
            'post_type' => 'vihje',
            'numberposts' => max($vihje_count - $vcount, 1),
            'meta_key'    => 'vihje_expires',
            'meta_type'   => 'DATETIME',
            'orderby'     => 'meta_value',
            'order'       => 'DESC',
            'meta_query' => array(
              array(
                'key'     => 'vihje_expires',
                'value'   => date_format(new DateTime('NOW', $tz), "Y-m-d H:i:s"),
                'compare' => '<=',
              ),
            )
          ));

          $vmerge = array_merge($vihjeet, $vihjeet_old);
          foreach ( $vmerge as $post ) {
            setup_postdata( $post );
            ?>
            <div class="max-w-full w-full md:w-1/2 xl:w-1/3 p-0 mb-4">
              <?php
              set_query_var('h', 'h3');
              get_template_part( 'template-parts/vihje-card-element' );
              set_query_var('h', null);
              ?>
            </div>
            <?php
          }
          wp_reset_postdata();
        ?>
        <div class="w-full flex justify-around">
          <a class="btn-base text-sm lg:text-xl" href="/vihjeet">
            Lisää vihjeitä
          </a>
        </div>

        <div class="w-full flex flex-wrap justify-between">
          <h2 class="w-full py-4 text-xl lg:text-2xl text-primary font-bold text-center font-display uppercase"><?php _e('Uusimmat tarjoukset', 'affi'); ?></h2>

          <?php
          $kamp_id = get_cat_ID('kampanjat');
          if ($kamp_id !== 0) {
            $kampanja_count = wp_is_mobile() ? 3 : 6;
            $kampanjat = get_posts( array(
              'post_type' => 'post',
              'category' => $kamp_id,
              'numberposts' => $kampanja_count
            ) );

            if ( $kampanjat ) {
              foreach ( $kampanjat as $post ) {
                setup_postdata( $post );
                ?>
                <div class="max-w-full lg:max-w-140 w-full md:w-1/2 xl:w-1/3 p-0 mb-4">
                  <?php
                  set_query_var('h', 'h3');
                  get_template_part( 'template-parts/kampanja-card-element' );
                  set_query_var('h', null);
                  ?>
                </div>
                <?php
              }
              wp_reset_postdata();
            }
          }
          ?>
          <div class="w-full flex justify-around">
            <a class="btn-base text-sm lg:text-xl" href="/kampanjat">
              Lisää tarjouksia
            </a>
          </div>
        </div>
        </div>
      </div>

    </main>
  </div>

  <?php get_footer(); ?>
</body>

</html>
