<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package TLC
 * @since TLC 0.8
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>
  <div class="mx-auto">
  <div class="mx-0 lg:mx-2 flex flex-row">

    <main id="main" class="fixed-content-width">
      <?php if ( have_posts() ) : ?>

        <header class="page-header">
          <h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'tlc-custom' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        </header><!-- .page-header -->

        <!-- use the search form that includes query vars -->
        <?php get_search_form(); ?>

        <?php
        while ( have_posts() ) {
          the_post();
          get_template_part( 'template-parts/list-element' );
        }
        ?>

      <?php else : ?>
        <h1 class="page-title"><?php printf( __( 'No search results for: %s', 'tlc-custom' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
        <!-- use the search form that includes query vars -->
        <?php get_search_form(); ?>
      <?php endif; ?>

    </main>
  </div>
  </div>

  <?php get_footer(); ?>
</body>
</html>
