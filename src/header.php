<?php
/**
 * Header for Affi theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" >


  <link rel="profile" href="https://gmpg.org/xfn/11" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

  <?php wp_head(); ?>
</head>

<body>
  <header id="site-header" class="font-display z-50 pb-0">
    <div class="w-full lg:hidden bg-primary">
      <?php if (is_front_page()): ?>
        <h1 class="lg:w-full">
      <?php endif; ?>
          <!-- This is only visible on mobile, on desktop this gets overriden by the meny nav link
               Similarly the h1 disapears on desktop because of this
          -->
          <div class="w-mobile-logo">
          <?php
          if ( function_exists( 'the_custom_logo' ) ) {
            the_custom_logo();
          }
          ?>
          </div>
      <?php if (is_front_page()): ?>
        </h1>
      <?php endif; ?>
    </div>
    <div class="bg-primary">
      <a class="skip-main" href="#main"><?php _e('Skip to content') ?></a>
      <nav class="w-full flex fixed lg:relative bottom-0 lg:bottom-auto bg-primary lg:px-0 mx-auto page-2xl">
        <div class="lg:hidden bottom-0 right-0 w-85p pt-4">
          <?php
          // TODO how to configure the icons? CSS :before?
          wp_nav_menu( array(
            'menu' => 'shortcut-menu',
            'theme_location' => 'shortcut-menu',
            'depth' => 1,
            'container' => false,
            'menu_class' => 'shortcut-menu justify-around',
            'link_before' => '<div><i class="font-ma-icons shortcut-icon not-italic text-2xl leading-none"></i></div>'
          ))
          ?>
        </div>
        <input type="checkbox" id="menyAvPaa" class="font-ma-icons leading-none text-2xl p-4 ml-auto w-15p" role="button"
               onchange="hideOverflow(this);"
               onkeypress="checkOnEnter(this, event);"
               aria-label="<?php _e('menu', 'affi'); ?>">
        </input>
        <div id="meny" class="w-full flex lg:justify-between bg-primary z-50 fixed lg:relative bottom-24 lg:bottom-0 right-0">
          <div class="flex w-auto items-start lg:items-center flex-shrink-0 text-black bg-primary lg:mr-6 lg:py-2">
            <?php if ( function_exists( 'the_custom_logo' ) ):?>
              <div class="w-mobile-logo lg:w-auto">
                <?php the_custom_logo(); ?>
              </div>
            <?php endif; ?>
          </div>
          <hr class="lg:hidden hr-line border border-white my-0" />
          <?php
          wp_nav_menu( array(
            'menu' => 'primary-menu',
            'theme_location' => 'primary-menu',
            'depth' => 3,
            'container' => false,
            'menu_class' => 'flex flex-col lg:flex-row items-start lg:items-center mr-2 ml-2 md:m-left-auto',
            'after' => '<input type="checkbox" role="button" class="icon open-sub-menu" aria-label="expand"></input>'
          ))
          ?>
        </div>
      </nav>
    </div>
  </header>
</body>

</html>
