<?php
/**
 */

/* Preload Google Fonts */
function themeprefix_load_fonts() {
  $font = 'Roboto|Roboto+Condensed';
    ?>
<link rel="dns-prefetch" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="anonymous">
<link rel="preload" href="https://fonts.googleapis.com/css?family=<?= $font; ?>" as="fetch" crossorigin="anonymous">
<script type="text/javascript">
!function(e,n,t){"use strict";var o="https://fonts.googleapis.com/css?family=<?= $font; ?>",r="__3perf_googleFontsStylesheet";function c(e){(n.head||n.body).appendChild(e)}function a(){var e=n.createElement("link");e.href=o,e.rel="stylesheet",c(e)}function f(e){if(!n.getElementById(r)){var t=n.createElement("style");t.id=r,c(t)}n.getElementById(r).innerHTML=e}e.FontFace&&e.FontFace.prototype.hasOwnProperty("display")?(t[r]&&f(t[r]),fetch(o).then(function(e){return e.text()}).then(function(e){return e.replace(/@font-face {/g,"@font-face{font-display:swap;")}).then(function(e){return t[r]=e}).then(f).catch(a)):a()}(window,document,localStorage);
</script>
    <?php
}
add_action( 'wp_head', 'themeprefix_load_fonts' );

//Remove JQuery migrate
function remove_jquery_migrate($scripts)
{
  if (!is_admin() && isset($scripts->registered['jquery'])) {
    $script = $scripts->registered['jquery'];

    if ($script->deps) { // Check whether the script has any dependencies
      $script->deps = array_diff($script->deps, array(
        'jquery-migrate'
      ));
    }
  }
}

add_action('wp_default_scripts', 'remove_jquery_migrate');

function site_scripts() {
  wp_enqueue_style('affi_style', get_template_directory_uri() . '/style.css');
  wp_enqueue_style('material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', [], false, false );

  wp_enqueue_script('betting_theme_js', get_template_directory_uri() . '/assets/js/betting.js', [], false, false);
}

add_action( 'wp_enqueue_scripts', 'site_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function twentytwenty_skip_link_focus_fix() {
  // The following is minified via `terser --compress --mangle -- assets/js/skip-link-focus-fix.js`.
  ?>
  <script>
  /(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
  </script>
  <?php
}
add_action( 'wp_print_footer_scripts', 'twentytwenty_skip_link_focus_fix' );

function register_my_menus() {
  register_nav_menus(
    array(
      'primary-menu' => __( 'Primary Menu' ),
      'shortcut-menu' => __( 'Mobile Shortcut Menu' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );

// allow shortcodes in text and html widgets
add_filter( 'widget_text', 'shortcode_unautop' );
add_filter( 'widget_text', 'do_shortcode' );

// Remove the admin bar since it doesn't play nice with the full width page
// TODO could add it back if we can get the CSS working with it
add_theme_support( 'admin-bar', array( 'callback' => '__return_false' ) );
// no hard-coded title tag
add_theme_support( 'title-tag' );

add_theme_support( 'post-thumbnails' );

add_theme_support( 'custom-logo', array( 'width' => 360, 'flex-height' => true ) );

function add_thumb_size() {
  add_image_size('list-thumb', 200, 0);
  add_image_size('grid-thumb', 400, 0);
  add_image_size('grid-thumb-lg', 600, 0);
}
add_action( 'after_setup_theme', 'add_thumb_size' );

function affi_widgets_init() {
  register_sidebar( array(
      'name'          => __( 'Sidebar', 'affi' ),
      'id'            => 'sidebar',
      'description'   => __( 'Widgets in this area will be shown on all pages except front-page.', 'affi' ),
      'before_widget' => '<li id="%1$s" class="widget %2$s">',
      'after_widget'  => '</li>',
      'before_title'  => '<h2 class="widgettitle">',
      'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'affi_widgets_init' );


/* Remove plugin template overrides */
function clear_plugin_templates() {
  remove_filter('single_template', 'affi_single_template');
}
add_action( 'init', 'clear_plugin_templates', 15 );

/**
 * Customizer options
 * - Logo
 * - Description text (hero-text)
 * - Title (hero-title)
 * - Feature image (hero-image)
 */
function your_theme_new_customizer_settings($wp_customize) {
  $wp_customize->add_setting('affi_theme_mailto', array('default' => ''));
  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'affi_theme_mailto',
    array(
      'label' => 'Email address',
      'section' => 'title_tagline',
      'settings' => 'affi_theme_mailto',
    )
  ));
}
add_action('customize_register', 'your_theme_new_customizer_settings');

// Fix a problem with old PHP version (7.0) on the server
// feature image is added to the excerpt so we remove it here.
add_filter('get_the_excerpt', 'filter_excerpt');

function filter_excerpt($param) {
  $start = strpos($param, '<img');
  $stop = strpos($param, '>', $start);

  if ($stop !== false) {
    return substr($param, $stop+1);
  } else {
    return $param;
  }
}

function betting_archive_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    }

    return $title;
}

add_filter( 'get_the_archive_title', 'betting_archive_title' );

// Modify the vihje archives to be based on expire date
function vihje_archive_per_page( $query ) {
  if ( !is_admin() && is_post_type_archive( 'vihje' ) && $query->is_main_query() ) {
    $query->set( 'meta_key', 'vihje_expires' );
    $query->set( 'meta_type', 'DATETIME' );
    $query->set( 'orderby', 'meta_value' );
    $query->set( 'order', 'DESC' );
  }
}
add_filter( 'pre_get_posts', 'vihje_archive_per_page' );

function betting_rewrite() {

  // hard-coded jalkapallon EM kisat page
  $id = 136078;
  add_rewrite_rule(
    '^jalkapallo/em-kisat/?$',
    'index.php?page_id=' . $id,
    'top'
  );

  // hard-coded jaakiekon MM kisat page
  $id = 139401;
  add_rewrite_rule(
    '^jaakiekko/mm-kisat/?$',
    'index.php?page_id=' . $id,
    'top'
  );

  $id = 137117;
  add_rewrite_rule(
    '^jalkapallo/valioliiga/?$',
    'index.php?page_id=' . $id,
    'top'
  );
}
add_action('init', 'betting_rewrite');

function betting_button_shortcode( $atts, $content = null ) {

  // Extract shortcode attributes
  extract( shortcode_atts( array(
    'url'    => '',
    'title'  => '',
    'target' => '',
    'text'   => '',
    'rel'    => '',
  ), $atts ) );

  // Use text value for items without content
  $content = $text ? $text : $content;

  // Return button with link
  $btn_cls = "w-full inline-block md:w-auto btn btn-cta bg-cta py-2 px-8 darken-on-hover mb-12 text-center text-white text-lg font-bold uppercase leading-tight";
  $link_attrs_str = '';
  if ( $url ) {

    $link_attr = array(
      'href'   => esc_url( $url ),
      'target' => ( 'blank' == $target || '_blank' == $target ) ? '_blank' : '',
      'rel'    => esc_attr( $rel ),
    );

    foreach ( $link_attr as $key => $val ) {
      if ( $val ) {
        $link_attrs_str .= ' ' . $key . '="' . $val . '"';
      }
    }
  }
  return '<a' . $link_attrs_str . " class=\"$btn_cls\">" . do_shortcode( $content ) . '</a>';
}
add_shortcode( 'button', 'betting_button_shortcode' );
