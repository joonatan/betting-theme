<?php
/**
 * Template Name: No sidebar
 * - 750px wide
 * - no sidebar
 * - otherwise same as singular
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>
  <div class="mx-0 flex justify-center mt-0 lg:mt-6">
  <div class="page-2xl mx-0 lg:mx-2 flex flex-col lg:flex-row flex-grow">

    <main id="main" class="lg:page-md flex-grow lg:mr-4" tabindex="-1">
      <?php get_template_part( 'template-parts/render_all_posts' ); ?>
    </main>

  </div>
  </div>

  <?php get_footer(); ?>
</body>
</html>

