<?php
/**
 */

?><!DOCTYPE html>

<!-- TODO
     - need one with a search button (icon) and a clear button (X)
 -->
<form role="search" method="get" class="search-form" action="<?php echo esc_url( get_site_url() ); ?>">
    <i class="fas fa-search"></i>
    <!-- @todo the message should be based on where we are searching (query vars) -->
    <input type="search" class="search-field w-full" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'local search', 'affi' ) ?>" value="<?php echo get_search_query() ?>" name="s" />
    <input type="hidden" class="search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button', 'affi' ) ?>" />
    <?php if (!empty(get_query_var('post_type'))): ?>
      <input type="hidden" name="post_type" value="<?php echo get_query_var('post_type'); ?>" />
    <?php endif; ?>
</form>
