// mobile navigation aid, hide overflow when the nav modal is shown
const hideOverflow = (evt) => {
  if (evt.checked) {
    document.body.style.overflowY='hidden';
  } else {
    document.body.style.overflowY='auto';
  }
}

/* Accept enter key for checking a checkbox */
const checkOnEnter = (elem, event) => {
  if (event.keyCode == 13) {
    elem.checked = !elem.checked;
    elem.onchange.apply(elem);
  }
}
