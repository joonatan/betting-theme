<?php
/**
 * Footer for Affi theme
 */

?>
<footer id="site-footer" class="pb-24 lg:pb-0 flex flex-col md:flex-row justify-between">
  <div class="m-2 text-center pt-4 text-white">
    <?php if (!empty(get_theme_mod( 'affi_theme_mailto' ))): ?>
      <a href="mailto:<?php echo get_theme_mod( 'affi_theme_mailto' ); ?>">
         <?php echo get_theme_mod( 'affi_theme_mailto' ); ?>
      </a>
    <?php endif; ?>
    <p>
      <?= esc_attr( get_bloginfo( 'name', 'display' ) ); ?>
      &copy; <?= date_format(new DateTime(), 'Y'); ?>
    </p>
  </div>

  <div class="m-2 text-center pt-4 text-white">
    <p style='font-size:0.8em;'>18+ Pelaathan vastuullisesti
      <a  style='color:white;' href='https://www.gamblingtherapy.org/' rel="noopener" target="_blank">
        Gamblingtherapy.org
      </a>
    </p>
  </div>

  <div class="m-2">
    <?php
    if ( function_exists( 'the_custom_logo' ) ) {
      the_custom_logo();
    }
    ?>
  </div>

</footer>
<?php wp_footer(); ?>
