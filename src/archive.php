<?php
/**
 *
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>
  <div class="mx-0 flex justify-center mt-2">

    <main id="main" class="archive page-xl flex flex-col bg-graa py-2 lg:py-0 mx-2" tabindex="-1">
      <h1 class="w-full uppercase bg-primary text-white text-center text-2xl py-0 mb-4"><?= get_the_archive_title(); ?></h1>

      <div class="w-full flex md:mx-0 flex-wrap space-between">
      <?php
      if ( have_posts() ):
        while ( have_posts() ):
          the_post();
          ?>
          <div class="max-w-full w-full lg:w-1/2 xl:w-1/3 p-0 mb-4">
            <?php
            if (get_post_type() == 'vihje'):
              get_template_part( 'template-parts/vihje-card-element' );
            else:
              get_template_part( 'template-parts/card-element' );
            endif;
            ?>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
      </div>
      <nav class="w-full flex justify-between my-4">
        <div><?php previous_posts_link( '<div class="btn-sec-sm">' . __('Newer posts') . '</div>' ); ?></div>
        <div><?php next_posts_link( '<div class="btn-sec-sm">' . __('Older posts') . '</div>' ); ?></div>
      </nav>
      <?php $desc = wpautop( apply_filters( 'the_content', term_description() ) ); ?>
      <?php if ( !is_paged() && !empty($desc) ): ?>
      <div class="w-full py-2 my-2 mb-4 bg-blue-200">
        <div class="page-md md:mx-auto">
          <?= wpautop( apply_filters( 'the_content', term_description() ) ); ?>
        </div>
      </div>
      <?php endif; ?>
    </main>
    <!-- TODO sidebar doeesn't work on mobile -->
    <aside class="hidden md:block">
      <?php get_sidebar(); ?>
    </aside>

  </div>

  <?php get_footer(); ?>
</body>

</html>
