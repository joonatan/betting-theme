<?php
  global $post;

  $header = get_query_var('h', 'h2');
  $font_size = is_front_page() ? ' text-lg lg:text-2xl ' : ' text-lg lg:text-lg';
?>

<!-- need zero margin and padding so that the percentage width works properly -->
<a class="font-display relative min-h-60 flex flex-col mx-0 md:mr-4 bg-primary h-full rounded-b-xl focus:filter focus:brightness-90 hover:filter hover:brightness-90"
   href="<?php echo esc_url( get_permalink() ); ?>"
   >
  <div class="w-screen md:w-full">
    <?php the_post_thumbnail('medium_large'); ?>
  </div>
  <div class="card-text m-2 lg:mx-8">
    <?php
      echo "<$header class=\"text-white $font_size font-bold leading-tight p-0 m-0\">";
      the_title();
      echo "</$header>";
    ?>
  </div>

  <div class="w-auto flex flex-col absolute top-2 left-2">
    <?php $cats = get_the_category($post); ?>

    <?php if ( !empty($cats) ): ?>
    <div class="w-full h-auto overflow-hidden my-auto ">
      <div class="w-full flex flex-row justify-between">
      <?php foreach ( $cats as $c ):
        $cat = get_category( $c );
        ?>
        <div class="bg-primary text-white uppercase text-sm lg:text-lg font-bold rounded-full px-2">
           <?php echo $cat->name; ?>
        </div>
      <?php endforeach; ?>
      </div>
    </div>
    <?php endif; ?>
  </div>
</a>
