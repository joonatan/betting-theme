<?php
$cats = array();
$cats = array_filter( wp_get_post_categories( get_the_ID() ), function ($x) {
  $cat = get_category($x);
  if (strtolower($cat->name) != 'uncategorized') {
    return true;
  } else {
    return false;
  }
});

$bg = 'bg-primary';
$cat = null;
if ( !empty($cats) ) {
  $cat = get_category( $cats[0] );
  foreach ($cats as $c) {
    $c_ = get_category( $c );
    if ($c_->slug == 'kampanjat') {
      $bg = 'bg-kampanjat';
    }
  }
}

?>

<div class="page-lg font-display mb-3 min-h-20 last:mb-0 md:mr-2 flex flex-row focus:shadow-lg hover:shadow-lg">
  <div class="flex flex-col">
    <div class="w-44 h-full lg:w-60 text-center overflow-hidden <?= $bg; ?> rounded-xl">
      <?php if ( $cat ): ?>
      <a class="h-full w-full flex flex-col justify-center uppercase text-white m-0"
         href="<?php echo esc_url( get_category_link( $cat->term_id ) ); ?>"
         >
         <span><?php echo $cat->name; ?></span>
      </a>
      <?php endif; ?>
    </div>
  </div>

  <a class="w-full h-full bg-white flex flex-col md:flex-row"
     href="<?php echo esc_url( get_permalink() ); ?>">

    <div class="w-full rounded-xl p-2 pl-8 lg:pl-12 h-full flex flex-col justify-center leading-normal">
      <h2 class="text-primary text-base lg:text-xl font-bold p-0 leading-tight"><?php the_title(); ?></h2>
    </div>
  </a>
</div>
