<?php
  global $post;

  $header = get_query_var('h', 'h2');

  $is_vihje = get_post_type() === 'vihje';
  $date = get_the_date('j F Y - H:i');
  $filter_cls = '';
  if ($is_vihje) {
    $tz = new DateTimeZone('Europe/Helsinki');
    $expires = date_create($post->vihje_expires, $tz);

    // TODO In reality we want to filter out the expired ones
    // Show countdown for vihjeet
    $now = date_create();
    if ($expires > $now) {
      $interval = $now->diff($expires);
      $d = $interval->format('%a');
      $h = $d * 24 + $interval->format('%H');
      $m = $interval->format('%I');
      $date = "$h:$m";
    } else {
      $date = 'SULJETTU';
      $filter_cls = 'grayscale';
    }
  }

  // Get the tags containing the sports and the league
  // only a single tag (the league) is shown
  $league = null;
  $tags = get_the_tags($post);
  if ( !empty($tags) ) {
    $c = $tags[0];
    $cat = get_category( $c );
    $league = $cat->name;
  }
?>

<!-- need zero margin and padding so that the percentage width works properly -->
<!-- TODO: check the iPad / Half-FD breakpoints -->
<a class="font-display flex flex-row md:flex-col h-full max-w-full font-bold mx-0 md:mr-4 relative filter <?= $filter_cls; ?> focus:brightness-90 hover:brightness-90"
   href="<?php echo esc_url( get_permalink() ); ?>"
   >
  <div class="w-2/5 md:w-full">
    <?php the_post_thumbnail( wp_is_mobile() ? 'list-thumb' : 'grid-thumb-lg' ); ?>
  </div>

  <div class="flex flex-col w-3/5">
    <div class="w-full flex flex-col">
      <div class="flex flex-row justify-between bg-primary text-md mx-4 text-white rounded-2xl px-2 uppercase">
        <?php if ( $league ): ?>
          <div class="bg-primary md:bg-white text-white md:text-primary px-2 md:pt-2 md:pb-1 rounded-lg md:absolute md:top-4 md:left-4">
             <?= $league; ?>
          </div>
        <?php endif; ?>
        <!-- TIMER / expired -->
        <?php if (!empty($date)): ?>
          <div class="px-2 relative md:absolute bg-primary md:bottom-20 md:right-0"><?= $date; ?></div>
        <?php endif; ?>
      </div>

      <div class="w-full md:absolute bottom-0 left-0 md:bg-white md:opacity-75 leading-tight">
        <?php
          echo "<$header class=\"text-lg md:text-md text-primary p-0 m-2 mx-4\">";
          the_title();
          echo "</$header>";
        ?>
      </div>
    </div>
  </div>
</a>
