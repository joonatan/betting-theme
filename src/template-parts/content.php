<?php
/**
 * Template for displaying single page content
 *
 * Part of Affi theme
 */
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

  <?php $no_img = get_query_var('no_feature_image', false); ?>

  <?php if ( !$no_img && has_post_thumbnail() && ! post_password_required() ): ?>

      <figure class="feature-media">
        <div class="featured-media-inner">
          <?php
          the_post_thumbnail();
          // no support for captions for feature media
          // maybe put them under the title (clued to the media)?
          ?>
        </div><!-- .featured-media-inner -->
      </figure><!-- .featured-media -->

  <?php endif; ?>

  <header class="entry-header mb-4">
    <div class="entry-header-inner bg-gradient-to-b from-primary to-secondary border-t-2 border-white rounded-b-lg text-white pb-8">
      <?php
      $title_cls = 'font-display text-2xl leading-tight font-bold pt-0 mb-8 mt-4 mx-4 ';
      $title_cls .= is_page() ? 'text-center ' : 'text-left ';
      the_title( "<h1 class=\"$title_cls pt-4 mx-2 mb-4\">", '</h1>' );
      if (!is_page()):
      ?>
      <div>
        <div class="font-body text-sm text-right pr-4">
          <span class="material-icons align-middle" style="font-size: 1.3rem;">schedule</span>
          <span class="align-middle"><?= get_the_date('j F Y - H:i'); ?></span>
        </div>
        <div class="text-sm text-right pr-4">
          <span class="material-icons align-middle" style="font-size: 1.3rem;">person</span>
          <span class="align-middle"><?= get_the_author_meta('display_name'); ?></span>
        </div>
      </div>
      <?php endif; ?>
    </div><!-- .entry-header-inner -->
  </header><!-- .entry-header -->

  <div class="post-inner">
    <div class="entry-content mx-6">
      <?php the_content( __( 'Continue reading', 'twentytwenty' ) ); ?>

      <?php if ( is_single() ): ?>
      <div class="m-4">
        <h3>
          Liity keskustelemaan vedonlyönnistä:
        </h3>
        <p>
          <a target="_blank" href="https://www.facebook.com/groups/1316573578488324">Urheiluvedot FB-ryhmä</a> <br />
          <a target="_blank" href="https://t.me/urheiluvedot">Telegram</a> <br />
          <a target="_blank" href="https://discord.gg/Aqs47h64cV">Discord</a> <br />
        </p>
      </div>
      <?php endif; ?>
    </div><!-- .entry-content -->
  </div><!-- .post-inner -->

  <!-- add edit section -->
  <?php if ( is_user_logged_in() ) : ?>
  <div class="py-4 flex flex-row w-full justify-between uppercase">
    <?php edit_post_link( __( 'Edit', 'betting' ), '<div class="text-center mx-auto">', '</div>', null, 'btn btn-pri'); ?>
  </div>
  <?php endif; ?>

  <?php
  /**
   *  Output comments wrapper if it's a post, or if comments are open,
   * or if there's a comment number – and check for password.
   * */
  if ( ( is_single() || is_page() ) && ( comments_open() || get_comments_number() ) && ! post_password_required() ) {
    ?>

    <div class="comments-wrapper section-inner">
      <!-- @todo comments template isn't Responsive -->
      <?php // comments_template(); ?>
    </div><!-- .comments-wrapper -->

    <?php
  }
  ?>

</article><!-- .post -->
