<?php
$bookers = [];
$fields = ['first', 'second', 'third'];
foreach($fields as $arr_name):
  if (have_rows($arr_name)):
    while ( have_rows($arr_name) ):
      the_row();
      $arr = get_row();
      $post_id = -1;
      $booker = '';
      $multiplier = 0;
      foreach (array_keys($arr) as $elem) {
        $obj = get_sub_field_object($elem);
        $name = $obj['label'];
        $id = $obj['ID'];
        $val = $obj['value'];
        if ($name === 'Booker') {
          $post_id = $val;
          $booker = get_the_title($post_id);
        } else {
          $multiplier = (float) $val;
        }
      }
      array_push( $bookers, array('id' => $post_id, 'booker' => $booker, 'multiplier' => $multiplier) );
    endwhile;
  endif;
endforeach;
?>

<?php

// clear out empty elements
$bookers = array_filter( $bookers, function ($b) { return $b['id']; } );

// TODO filter out same ids (filter) (if the same booker is twice here)

// sort by multiplier
usort( $bookers, function ($a, $b) {
  if ( $a['multiplier'] === $b['multiplier'] ) {
    return 0;
  }
  return ($a['multiplier'] < $b['multiplier']) ? 1 : -1;
});

if (count($bookers) > 0):
$first = $bookers[0];

// solution with these modals is to create one modal
// for every element in it
// iterate over bookers (all of them)
//  use booker_id as the id of the modal
//  then link it to the href
foreach ($bookers as $b):
  $url = get_field('cta', $b['id']);
  $logo = get_field('logo', $b['id']);
  $circ = get_field('circulation', $b['id']);
  $mult = get_field('multiplier', $b['id']);
  $deposit = get_field('deposit', $b['id']);
  $bonus = get_field('bonus', $b['id']);
  $expire_days = get_field('expire_days', $b['id']);
  $min_deposit = get_field('min_deposit', $b['id']);
  $t_b = get_field('t_b', $b['id']);
  $max_bet = get_field('max_bet', $b['id']);
  $huom = get_field('huom', $b['id']);
?>
<!--
     @todo add close to right top corner
  // TODO do we want a link to the review (our page for the affi) here?
-->
<div id="bookerModal-<?= $b['id']; ?>" class="booker-modal">
  <div class="rounded-md border-2 border-solid border-blue">
    <div class="p-4 ">
      <h2 class="text-2xl font-bold"><?= $b['booker']; ?></h2>
      <img class="" src="<?= $logo; ?>" />
      <div class="text-2xl text-center font-semibold mb-4"><?= $deposit; ?> &euro; / <?= $bonus; ?> % bonus</div>
      <div class="text-lg font-bold">Kierrätys: <?= $circ; ?>-kertainen (<?= $t_b; ?>)</div>
      <div class="text-lg font-bold">Kierrätyskerroin: <?= $mult; ?></div>
      <div class="text-lg font-bold">Bonus voimassa: <?= $expire_days; ?> pv</div>
      <div class="text-lg font-bold">Minimitalletus: <?= $min_deposit; ?> &euro;</div>
      <div class="text-lg font-bold mb-4">Maksimipanos: <?= $max_bet; ?> &euro; / veto</div>
      <p class="mb-4"><?= $huom; ?></p>
      <div class="flex">
        <a class="w-1/2 border-2 border-blue border-solid hover:bg-blue hover:text-white flex" title="Takaisin" href="#ok">
          <span class="w-full self-center text-xl uppercase px-4">Takaisin</span>
        </a>
        <a class="w-1/2 bg-cta hover:bg-blue hover:text-white flex" title="Pelaa" href="<?= $url; ?>">
           <span class="w-full self-center text-center text-2xl font-bold uppercase px-4">
            Pelaa
          </span>
        </a>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>

<?php // Draw the expire info ?>
<?php
// Add timezone info: all our times are from Helsinki
$tz = new DateTimeZone('Europe/Helsinki');
$d = new DateTime(get_field('expires'), $tz);
$now = new DateTime('now', $tz);
if ( $d > $now ):
?>
  <div class="text-green-700 font-bold text-lg flex justify-between mx-4">
    <div>Sulkeutuu</div>
    <div class="inline-block text-right">
      <?php
      echo __(date_format($d, "l"));
      echo '<br />';
      echo date_format($d, " j ");
      echo __(date_format($d, "F"));
      echo date_format($d, " Y");
      echo '<br />';
      echo date_format($d, "H:i");
      ?>
    </div>
  </div>
<?php else: ?>
  <div class="text-red-500 font-bold text-xl">
    Sulkeutunut
  </div>
<?php endif; ?>

<?php // Draw the header ?>
<div class="flex flex-col">
  <!-- TODO this needs to have game name and what else? -->
  <div class="flex flex-row border-none bg-primary w-full">
    <div class="text-white font-bold text-xl mx-4">
      <?= get_field('game'); ?>
    </div>
    <div class="mx-auto"></div>
    <div class="w-2/12 md:w-1/4 text-white font-bold text-xl mx-4">
      <span class="hidden md:inline">Panostus: </span><?= get_field('panostus'); ?>
    </div>
  </div>
<?php // Draw the first element using a different style ?>
<?php
  $url = get_field('cta', $first['id']);
  $logo = get_field('logo', $first['id']);
?>
  <div class="flex flex-row flex-wrap md:flex-nowrap justify-between items-center border-b-2 border-primary border-solid">
    <div class="w-auto md:w-2/12 text-2xl font-bold text-blue text-center py-1">
      <span class="hidden md:inline">@ </span><?= number_format($first['multiplier'], 2); ?>
    </div>
    <div class="w-1/2 md:w-5/12 ">
      <img class="booker-logo" src="<?= $logo; ?>" />
    </div>
    <a class="w-full md:w-2/12 order-last md:order-none text-white bg-blue hover:bg-primary hover:text-white self-stretch flex px-2"
       href="#bookerModal-<?= $first['id']; ?>">
      <span class="w-full self-center text-center text-xl uppercase">
      Ehdot
      </span>
    </a>
    <!-- @todo this should have a blue border, but it messes up the fixed width -->
    <!-- use flex to center the text element inside a varried sized button -->
    <a class="w-auto md:w-3/12 bg-cta hover:bg-primary hover:text-white self-stretch text-center text-black text-xl font-bold uppercase flex px-2"
         href="<?= $url; ?>">
        <span class="w-full self-center">Pelaa</span>
    </a>
  </div>
<?php foreach (array_slice($bookers, 1) as $b): ?>
<?php
  $url = get_field('cta', $b['id']);
  $logo = get_field('logo', $b['id']);
  $circ = get_field('circulation', $b['id']);
  $mult = get_field('multiplier', $b['id']);
  $deposit = get_field('deposit', $b['id']);
  $bonus = get_field('bonus', $b['id']);
  $t_b = get_field('t_b', $b['id']);
?>
  <div class="flex flex-row justify-between items-center">
    <div class="w-1/2 md:w-5/12">
      <img class="booker-logo" src="<?= $logo; ?>" />
    </div>
    <div class="w-auto md:w-1/12 order-first md:order-none text-lg font-bold text-blue text-center py-1 m-2 mr-1">
      <?= number_format($b['multiplier'], 2); ?>
    </div>
    <div class="w-5/12 md:auto font-semibold text-sm text-center py-1 m-2">
      <?= $deposit; ?>&euro; / <?= $bonus; ?>% | <?= $circ; ?>x
      <span class="hidden md:inline"> <?= $t_b; ?> |</span> <?= $mult; ?>
    </div>
    <a class="hidden md:inline w-2/12 border-2 border-blue border-solid text-blue hover:text-white hover:bg-blue py-2"
       href="#bookerModal-<?= $b['id']; ?>">
      <span class="block text-center text-lg uppercase px-2 md:px-4 font-semibold">
      Ehdot
      </span>
    </a>
    <a class="w-1/5 bg-cta border-2 border-black border-solid text-black hover:bg-blue hover:text-white py-2"
         href="<?= $url; ?>">
      <span class="block text-center text-lg font-bold uppercase px-2 md:px-4">
        Pelaa
      </span>
    </a>
  </div>
<?php endforeach; ?>
</div>

<?php endif; ?>
