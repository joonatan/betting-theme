<?php
  global $post;

  $header = get_query_var('h', 'h2');
?>

<!-- need zero margin and padding so that the percentage width works properly -->
<a class="font-display relative flex flex-row md:flex-col mx-0 md:mr-4 h-full focus:filter focus:brightness-90 hover:filter hover:brightness-90"
   href="<?php echo esc_url( get_permalink() ); ?>"
   >
  <div class="w-5/12 md:w-full">
    <?php the_post_thumbnail( 'grid-thumb-lg' ); ?>
  </div>

  <div class="w-7/12 md:w-full md:absolute bottom-0 left-0 md:bg-white md:opacity-80 leading-tight">
    <?php
      echo "<$header class=\"text-primary text-lg md:text-base font-bold p-0 mx-4\">";
      the_title();
      echo "</$header>";
    ?>
  </div>
</a>
