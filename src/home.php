<?php
/**
 * Template for latests posts
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>
  <div class="mx-0 flex justify-center mt-2">

    <main id="main" class="archive page-xl flex flex-col bg-graa py-2 lg:py-0 mx-2" tabindex="-1">
      <h1 class="w-full uppercase bg-primary text-white text-center text-2xl py-0 mb-4">Uutiset</h1>

      <div class="w-full flex md:mx-0 flex-wrap space-between">
      <?php
      while( have_posts() ):
        the_post();
        ?>
        <div class="max-w-full w-full lg:w-1/2 xl:w-1/3 p-0 mb-4">
          <?php get_template_part( 'template-parts/card-element' ); ?>
        </div>
      <?php endwhile; ?>
      </div>
      <nav class="flex justify-between my-4">
        <div><?php previous_posts_link( '<div class="btn-sec-sm">' . __('Newer posts') . '</div>' ); ?></div>
        <div><?php next_posts_link( '<div class="btn-sec-sm">' . __('Older posts') . '</div>' ); ?></div>
      </nav>
    </main>

    <aside class="hidden md:block">
      <?php get_sidebar(); ?>
    </aside>

  </div>

  <?php get_footer(); ?>
</body>

</html>
