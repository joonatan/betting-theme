<?php
/*
 * Template Name: Full Width
 * - 1200px wide
 * - no sidebar
 * - no feature image
 * */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>
  <div class="mx-0 flex justify-center mt-0 lg:mt-6">
  <div class="page-2xl mx-0 lg:mx-2 flex flex-col lg:flex-row flex-grow">

    <main id="main" class="lg:page-xl flex-grow lg:mr-4" tabindex="-1">
      <?php
      set_query_var('no_feature_image', true);
      get_template_part( 'template-parts/render_all_posts' );
      set_query_var('feature_image', null);
      ?>
    </main>
  </div>
  </div>

  <?php get_footer(); ?>

</body>
