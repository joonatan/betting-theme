<?php
/**
 * The template for displaying the 404 template in the TLC theme.
 *
 */
 ?>


<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body>

  <div class="flex">

    <main id="main" class="content ml-10 mr-auto w-full">
      <h1 class="entry-title"><?php _e( 'Page Not Found', 'twentytwenty' ); ?></h1>
      <div class="intro-text">
        <p><?php _e( 'The page you were looking for could not be found. It might have been removed, renamed, or did not exist in the first place.', 'twentytwenty' ); ?></p>
      </div>

      <?php
      get_search_form(
        array(
          'label' => __( '404 not found', 'twentytwenty' ),
        )
      );
      ?>
    </main>
  </div>

  <?php get_footer(); ?>
</body>
</html>
